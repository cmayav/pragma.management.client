package co.com.pragma.managementclient.controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMappingException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import co.com.pragma.managementclient.dto.ClientDTO;
import co.com.pragma.managementclient.entity.Client;
import co.com.pragma.managementclient.utilities.DbConnection;

public class DeleteController implements RequestHandler<ClientDTO, Boolean> {

	@Override
	public Boolean handleRequest(ClientDTO input, Context context) {

		final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		try {
			DynamoDBMapper mapper = new DynamoDBMapper(DbConnection.CLIENT);
			if (input != null && input.getId() > 0) {
				Client c = mapper.load(Client.class, input.getId());
				mapper.delete(c);
				return true;
			}
		} catch (DynamoDBMappingException e) {
			logger.log(Level.SEVERE, "(1) Error DynamoDb " + e.getMessage());
			return false;
		} catch (Exception e) {
			logger.log(Level.SEVERE, "(2)" + e.getMessage());
			return false;
		}
		return false;
	}
}
