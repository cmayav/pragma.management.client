package co.com.pragma.managementclient.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMappingException;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import co.com.pragma.managementclient.dto.ClientDTO;
import co.com.pragma.managementclient.entity.Client;
import co.com.pragma.managementclient.utilities.ClientConverter;
import co.com.pragma.managementclient.utilities.DbConnection;

public class FindByController implements RequestHandler<ClientDTO, List<ClientDTO>> {

	@Override
	public List<ClientDTO> handleRequest(ClientDTO input, Context context) {

		DynamoDBMapper mapper = new DynamoDBMapper(DbConnection.CLIENT);
		List<Client> lstClient = new ArrayList<>();
		final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {

			DynamoDBScanExpression query = new DynamoDBScanExpression();

			// tipoIdentificacion: 1.RC | 2.TI | 3.CC | 4.CE | 5.PA
			if (input.getTipoIdentificacion() > 0 && input.getTipoIdentificacion() <= 5) {
				query.addFilterCondition("tipoIdentificacion",
						new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(
								new AttributeValue().withN(String.valueOf(input.getTipoIdentificacion()))));
			}

			if (input.getIdentificacion() != null && !input.getIdentificacion().equals("")) {
				query.addFilterCondition("identificacion",
						new Condition().withComparisonOperator(ComparisonOperator.CONTAINS)
								.withAttributeValueList(new AttributeValue().withS(input.getIdentificacion())));
			}

			// filtroEdad: 1=EQ : Equal | 2=LE : Less than or equal | 3=LT : Less than |
			// 4=GE : Greater than or equal | 5=GT : Greater than
			if ((input.getFiltroEdad() >= 1 && input.getFiltroEdad() <= 5) && input.getEdad() > 0) {

				AttributeValue attEdad = new AttributeValue().withN(String.valueOf(input.getEdad()));

				switch (input.getFiltroEdad()) {
				case 1:
					query.addFilterCondition("edad", new Condition().withComparisonOperator(ComparisonOperator.EQ)
							.withAttributeValueList(attEdad));
					break;

				case 2:
					query.addFilterCondition("edad", new Condition().withComparisonOperator(ComparisonOperator.LE)
							.withAttributeValueList(attEdad));
					break;

				case 3:
					query.addFilterCondition("edad", new Condition().withComparisonOperator(ComparisonOperator.LT)
							.withAttributeValueList(attEdad));
					break;

				case 4:
					query.addFilterCondition("edad", new Condition().withComparisonOperator(ComparisonOperator.GE)
							.withAttributeValueList(attEdad));
					break;

				case 5:
					query.addFilterCondition("edad", new Condition().withComparisonOperator(ComparisonOperator.GT)
							.withAttributeValueList(attEdad));
					break;

				default:
					break;
				}
			} else {
				if (input.getEdad() > 0) {
					AttributeValue attEdad = new AttributeValue().withN(String.valueOf(input.getEdad()));
					query.addFilterCondition("edad", new Condition().withComparisonOperator(ComparisonOperator.EQ)
							.withAttributeValueList(attEdad));
				}
			}

			lstClient = mapper.scan(Client.class, query);
			ClientConverter converter = new ClientConverter();
			return converter.fromEntity(lstClient);

		} catch (DynamoDBMappingException e) {
			logger.log(Level.SEVERE, "Error DynamoDb " + e.getMessage());
			return new ArrayList<>();
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
			return new ArrayList<>();
		}
	}

}
