package co.com.pragma.managementclient.controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMappingException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import co.com.pragma.managementclient.dto.ClientDTO;
import co.com.pragma.managementclient.entity.Client;
import co.com.pragma.managementclient.utilities.DbConnection;

public class UpdateController implements RequestHandler<ClientDTO, ClientDTO> {

	@Override
	public ClientDTO handleRequest(ClientDTO input, Context context) {

		final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {

			DynamoDBMapper mapper = new DynamoDBMapper(DbConnection.CLIENT);

			if (input != null && input.getId() > 0 && input.getTipoIdentificacion() > 0
					&& !input.getIdentificacion().equals("") && !input.getNombres().equals("")
					&& !input.getApellidos().equals("") && input.getEdad() > 0) {
				Client clientEntity = mapper.load(Client.class, input.getId());
				if (clientEntity != null) {
					clientEntity.setTipoIdentificacion(input.getTipoIdentificacion());
					clientEntity.setIdentificacion(input.getIdentificacion());
					clientEntity.setNombres(input.getNombres());
					clientEntity.setApellidos(input.getApellidos());
					clientEntity.setEdad(input.getEdad());
					clientEntity.setCiudadNacimiento(input.getCiudadNacimiento());
					mapper.save(clientEntity);
					return input;
				}
			}
		} catch (DynamoDBMappingException e) {
			logger.log(Level.SEVERE, "Error DynamoDb " + e.getMessage());
			return null;
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
			return null;
		}
		return null;
	}

}
