package co.com.pragma.managementclient.utilities;

import java.util.List;

public interface IConverter<E, D> {

	public abstract E fromDTO(D dto);

	public abstract D fromEntity(E entity);

	public List<E> fromDTO(List<D> dtos);

	public List<D> fromEntity(List<E> entities);

}
