package co.com.pragma.managementclient.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@DynamoDBTable(tableName = "Clientes")
public class Client {

	@DynamoDBHashKey(attributeName = "id")
	private long id;

	@DynamoDBAttribute(attributeName = "tipoIdentificacion")
	private int tipoIdentificacion;

	@DynamoDBAttribute(attributeName = "identificacion")
	private String identificacion;

	@DynamoDBAttribute(attributeName = "nombres")
	private String nombres;

	@DynamoDBAttribute(attributeName = "apellidos")
	private String apellidos;

	@DynamoDBAttribute(attributeName = "edad")
	private int edad;

	@DynamoDBAttribute(attributeName = "ciudadNacimiento")
	private int ciudadNacimiento;

}
