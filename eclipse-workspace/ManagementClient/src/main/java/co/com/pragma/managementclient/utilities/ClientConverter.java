package co.com.pragma.managementclient.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import co.com.pragma.managementclient.dto.ClientDTO;
import co.com.pragma.managementclient.entity.Client;

public class ClientConverter implements IConverter<Client, ClientDTO> {

	@Override
	public Client fromDTO(ClientDTO dto) {

		Client client = new Client();
		client.setId(dto.getId());
		client.setTipoIdentificacion(dto.getTipoIdentificacion());
		client.setIdentificacion(dto.getIdentificacion());
		client.setNombres(dto.getNombres());
		client.setApellidos(dto.getApellidos());
		client.setEdad(dto.getEdad());
		client.setCiudadNacimiento(dto.getCiudadNacimiento());

		return client;
	}

	@Override
	public ClientDTO fromEntity(Client entity) {

		ClientDTO clientDTO = new ClientDTO();
		clientDTO.setId(entity.getId());
		clientDTO.setTipoIdentificacion(entity.getTipoIdentificacion());
		clientDTO.setIdentificacion(entity.getIdentificacion());
		clientDTO.setNombres(entity.getNombres());
		clientDTO.setApellidos(entity.getApellidos());
		clientDTO.setEdad(entity.getEdad());
		clientDTO.setCiudadNacimiento(entity.getCiudadNacimiento());

		return clientDTO;
	}

	@Override
	public List<Client> fromDTO(List<ClientDTO> dtos) {
		if (dtos == null)
			return new ArrayList<>();
		return dtos.stream().map(dto -> fromDTO(dto)).collect(Collectors.toList());
	}

	@Override
	public List<ClientDTO> fromEntity(List<Client> entities) {
		if (entities == null)
			return new ArrayList<>();
		return entities.stream().map(entity -> fromEntity(entity)).collect(Collectors.toList());
	}

}
