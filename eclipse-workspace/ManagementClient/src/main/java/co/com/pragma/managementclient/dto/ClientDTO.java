package co.com.pragma.managementclient.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ClientDTO {
	
	private long id;
    private int tipoIdentificacion;
    private String identificacion;
    private String nombres;
    private String apellidos;    
    private int edad;
    private int ciudadNacimiento;
    private int filtroEdad;
    
}
