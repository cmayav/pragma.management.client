package co.com.pragma.managementclient.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMappingException;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import co.com.pragma.managementclient.dto.ClientDTO;
import co.com.pragma.managementclient.entity.Client;
import co.com.pragma.managementclient.utilities.ClientConverter;
import co.com.pragma.managementclient.utilities.DbConnection;

public class FindAllController implements RequestHandler<ClientDTO, List<ClientDTO>> {

	@Override
	public List<ClientDTO> handleRequest(ClientDTO input, Context context) {

		DynamoDBMapper mapper = new DynamoDBMapper(DbConnection.CLIENT);
		List<Client> lstClient = new ArrayList<>();
		List<ClientDTO> lstClientDTO = new ArrayList<>();

		DynamoDBScanExpression query = new DynamoDBScanExpression();
		final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {
			lstClient = mapper.scan(Client.class, query);

			lstClient.forEach(e -> {
				ClientDTO cDTO = new ClientDTO();
				ClientConverter converter = new ClientConverter();
				cDTO = converter.fromEntity(e);
				lstClientDTO.add(cDTO);
			});
		} catch (DynamoDBMappingException e) {
			logger.log(Level.SEVERE, "Error DynamoDb " + e.getMessage());
			return new ArrayList<>();
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
			return new ArrayList<>();
		}

		return lstClientDTO;
	}

}
