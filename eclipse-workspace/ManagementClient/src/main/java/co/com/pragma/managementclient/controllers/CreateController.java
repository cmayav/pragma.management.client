package co.com.pragma.managementclient.controllers;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMappingException;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import co.com.pragma.managementclient.dto.ClientDTO;
import co.com.pragma.managementclient.entity.Client;
import co.com.pragma.managementclient.utilities.ClientConverter;
import co.com.pragma.managementclient.utilities.DbConnection;

public class CreateController implements RequestHandler<ClientDTO, Client> {

	@Override
	public Client handleRequest(ClientDTO input, Context context) {

		final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		if (input != null && input.getTipoIdentificacion() > 0 && !input.getIdentificacion().equals("")
				&& !input.getNombres().equals("") && !input.getApellidos().equals("") && input.getEdad() > 0) {
			try {
				ClientConverter converter = new ClientConverter();
				Client client = converter.fromDTO(input);
				client.setId(new Date().getTime());

				DynamoDBMapper mapper = new DynamoDBMapper(DbConnection.CLIENT);
				mapper.save(client);
				return client;
			} catch (DynamoDBMappingException e) {
				logger.log(Level.SEVERE, "DynamoDBMappingException (1) " + e.getMessage());
				return null;
			} catch (ResourceNotFoundException e) {
				logger.log(Level.SEVERE, "ResourceNotFoundException (2) " + e.getMessage() + "table not found");
				return null;
			} catch (AmazonServiceException ase) {
				logger.log(Level.INFO, "Could not complete operation");
				logger.log(Level.SEVERE, "Error Message:  " + ase.getMessage());
				logger.log(Level.SEVERE, "HTTP Status:    " + ase.getStatusCode());
				logger.log(Level.SEVERE, "AWS Error Code: " + ase.getErrorCode());
				logger.log(Level.SEVERE, "Error Type:     " + ase.getErrorType());
				logger.log(Level.SEVERE, "Request ID:     " + ase.getRequestId());
			} catch (AmazonClientException ace) {
				logger.log(Level.SEVERE, "Internal error occurred communicating with DynamoDB");
				logger.log(Level.SEVERE, "Error Message:  " + ace.getMessage());
			} catch (Exception e) {
				logger.log(Level.SEVERE, "(3)" + e.getMessage());
				return null;
			}
		}
		return null;
	}

}
