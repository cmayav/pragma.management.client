package co.com.pragma.managementclient.utilities;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;

public class DbConnection {

	public static final AmazonDynamoDB CLIENT = AmazonDynamoDBClientBuilder.standard().build();
	public static final DynamoDB DYNAMODB = new DynamoDB(CLIENT);
	public static final String TABLE_NAME = "clientes";
	public static final Regions REGION = Regions.SA_EAST_1;

	private DbConnection() {
	}
}
